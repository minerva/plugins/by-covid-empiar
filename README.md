# BY-COVID EMPIAR MINERVA Plugin

This is the repository for a MINERVA plugin handling the [EMPIAR database entries specific for SARS-CoV-2](https://www.ebi.ac.uk/ebisearch/search?db=empiar&query=SARS-CoV-2&size=15).

The plugin is tailored  to be used with the COVID-19 Disease Map to visualise drug screening data available at the BioImage Archive. This dataset contains the effects of antiviral drugs and their effects on the SARS-CoV-2 infected cell lines.

**To launch the plugin:**
- go to the COVID-19 Disease Map hosted on the MINERVA Platform (https://covid19map.elixir-luxembourg.org/minerva/)
- load the plugin using the menu icon (three horizontal lines, upper left corner)
- Find the "EMPIAR Covid" plugin and press "Load" button (see below)

![load](./img/plugin_load.png)

**To use the plugin:**
- browse the list of SARS-CoC-2 related structures (rightmost panel)
- use the "+" icon to expand a given structure
- the expanded structure is displayed in the plugin panel, and its related molecules are dynamically loaded in the map area (middle panel)

![load](./img/plugin_example.png)

Many thanks to Aastha Mathur, Isabel Kemmer from tbe BY-COVID project ([by-covid.eu](https://by-covid.eu)) for their help with developing the plugin. 

The plugin was developed with the support of the EU Horizon funding, [grant agreement ID: 101046203](https://cordis.europa.eu/project/id/101046203)
