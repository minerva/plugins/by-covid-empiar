const minervaWrapper = require('minerva-wrapper');
const MinervaElement = minervaWrapper.minerva.Element

require('../css/styles.css');
require('../css/minerva.css');
require('../assets/boostrap-table/bootstrap-table.min.css');

const jq = require('jquery');

//bootstrap table requires jquery to be in the global scope
if (window.$ === undefined) {
    if (minerva && minerva.$ !== undefined) {
        window.$ = window.jQuery = window.jquery = minerva.$;
    } else {
        window.$ = window.jQuery = window.jquery = jq;
    }
} else {
    jQuery = global.$ = global.jQuery = window.$;
}
try {

// boostrap-table is required directly because when using via npm, the boostrapTable is for unknown reason not added to the $ object when deployed on minerva-dev
    require('../assets/boostrap-table/bootstrap-table.min');
    const {minerva} = require("minerva-wrapper");

    const pluginName = 'empiar';
    const pluginLabel = 'EMPIAR Covid';
    const pluginVersion = '1.1.0';
    const containerName = pluginName + '-container';

    const settings = {
        highlightOnSearch: false,
        elementColumnName: 'Target protein',
        drugColumnName: 'COVID.19.Disease.Map.target',
        dataSource: 'https://minerva-service.lcsb.uni.lu/plugins/bycovid-empiar/empiar_mapping.tsv',
        imageBaseLink: 'https://www.ebi.ac.uk/pdbe/emdb-empiar/entryIcons/',
        empiarBaseLink: 'https://www.ebi.ac.uk/empiar/',
        mapFields: {
            EMPIAR_ID: "empiar_id",
            EMPIAR_Link: "empiar_link",
            "COVID.19.Disease.Map.target": "target",
        },
    };

    const globals = {
        empData: {},
    };


// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

    let $container;
    let $empTable;
    let pluginContainer;

    const register = async function () {

        $(".tab-content").css('position', 'relative');

        pluginContainer = $(minervaWrapper.getElement());

        globals.projectId = minervaWrapper.getProjectId();
        let models = await minervaWrapper.getModels();
        globals.modelId = models[0].id;
        initPlugin().then(() => console.log('Plugin loaded'));
    };

    const unregister = function () {
        unregisterListeners();
        return deHighlightAll();
    };

    minervaWrapper.init({
        register: register,
        unregister: unregister,
        name: pluginLabel,
        version: pluginVersion,
        minWidth: 300,
        defaultWidth: 450,
        pluginUrl: 'https://minerva-service.lcsb.uni.lu/plugins/bycovid-empiar/plugin.js',
    }).then(r => {
    });


    async function initPlugin() {
        registerListeners();
        const $container = initMainContainer();
        await initDrugBioEntities();

        $container.find('.compounds-loading').hide();
        initMainPageStructure($container);
        initEmpiarTable($container);
    }

    function registerListeners() {
    }

    function unregisterListeners() {
        minervaWrapper.removeAllListeners();
    }

// ****************************************************************************
// ********************* MINERVA INTERACTION*********************
// ****************************************************************************

    function deHighlightAll() {
        return minervaWrapper.getHighlightedBioEntities().then(highlighted => {
            return minervaWrapper.hideBioEntity(highlighted.map(function (entry) {
                return {
                    element: {
                        id: entry.element.getId(),
                        modelId: entry.element.getModelId(),
                        type: "ALIAS"
                    },
                    type: "ICON",
                    bioEntity: entry.element,
                }
            }));
        });
    }

// ****************************************************************************
// ********************* PLUGIN STRUCTURE AND INTERACTION*********************
// ****************************************************************************

    function getContainerClass() {
        return containerName;
    }

    function initMainContainer() {
        $container = $(`<div class="${getContainerClass()}"></div>`).appendTo(pluginContainer);
        $container.append(`
        <div class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content border-danger">
                    <div class="modal-header bg-danger text-light">                        
                            <h4 class="modal-title">Error</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <p>Loading the dataset from <i>${settings.dataSource}</i> failed.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div> 
            
        <div class="panel card panel-default compounds-loading">
            <div class="panel-body card-body">  
                <i class="fa fa-circle-o-notch fa-spin"></i> Fetching data ... This might take several minutes
                if this is the first time the plugin is loaded for this map ...
            </div>        
        </div>
    `);

        return $container;
    }

    function initMainPageStructure(container) {

        container.append(`
        <table class="empiar-table"></table>
    `);
    }

    function linkFormatter(value, baseUrl) {
        return `<a href="${baseUrl}${value}" target="_blank">${value}</a>`;
    }

    function showSubRow(empiarId) {
        let idNum = empiarId.split('-')[1]
        let url = `${settings.imageBaseLink}${idNum}-l.gif`;
        return $(`<img src="${url}" class="empiar-center">`)
    }

    function initEmpiarTable($container) {
        $empTable = $container.find('.empiar-table');
        $empTable.bootstrapTable({
            columns: [{
                field: settings.mapFields['EMPIAR_ID'],
                title: 'EMPIAR_ID',
                searchable: true,
                sortable: true,
                formatter: (value) => linkFormatter(value, settings.empiarBaseLink)
            }, {
                field: settings.mapFields[settings.drugColumnName],
                title: 'COVID-19 Disease Map target',
                sortable: true,
                searchable: true
            }],
            search: true,
            responsive: true,
            detailView: true,
            detailViewByClicks: true,
            detailFormatter: (index, row) => showSubRow(row[settings.mapFields['EMPIAR_ID']]),
            data: globals.empData,
            sortName: settings.mapFields['EMPIAR_ID'],
            sortOrder: 'asc',
            onSearch: () => {
                settings.highlightOnSearch && highlightInMap($empTable);
            },
            onExpandRow: (index, row, $detail) => {
                const l = $empTable.bootstrapTable('getData').length;
                for (let i = 0; i < l; i++) {
                    if (i != index) {
                        $empTable.bootstrapTable('collapseRow', i)
                    }
                }
                highlightInMap($empTable, row);
            },
            onCollapseRow: (index, row, dv) => {
                deHighlightAll().then(() => {
                    return
                });
            },
        });

    }

    function highlightInMap($empTable, row = undefined) {
        let data;
        row === undefined ? data = $empTable.bootstrapTable('getData') : data = [row];
        return deHighlightAll().then(function () {
            let highlightDefs = data.map(e => {
                return e.bioEntities.map(bioEntity => {
                        let markers = [];
                        markers.push(getMarkerForBioEntity(bioEntity));
                        if (globals.parentConnections[bioEntity.getModelId()] !== undefined) {
                            globals.parentConnections[bioEntity.getModelId()].forEach(parentBioEntity => {
                                markers.push(getMarkerForBioEntity(parentBioEntity));
                            });
                        }
                        return markers;
                    }
                ).flat();
            }).flat();


            return minervaWrapper.showBioEntity(highlightDefs);
        });
    }

    function parseTsv(tsv, elements) {
        const columns = [];
        const data = [];

        tsv.split(/\r?\n/).forEach((line, i) => {

            if (line.trim() === '') return;

            let row = {};
            line.split("\t").forEach((item, j) => {
                i === 0 ? columns.push(settings.mapFields[item]) : row[columns[j]] = item;
            });
            if (i > 0) {
                const covidRef = row[settings.mapFields[settings.drugColumnName]].toLowerCase();
                if (elements[covidRef]) {
                    row["bioEntities"] = elements[covidRef];
                    data.push(row);
                } else {
                    console.log("Species Not Found " + covidRef);
                }
            }
        });
        return data;
    }

    async function initDrugBioEntities() {
        try {
            let csv = await jq.ajax(settings.dataSource);
            let results = await retrieveElements();
            let elements = [];
            for (let name in results) {
                if (Object.prototype.hasOwnProperty.call(results, name)) {
                    elements = elements.concat(results[name]);
                }
            }
            globals.parentConnections = await retrieveConnections(elements);
            globals.empData = parseTsv(csv, results);
        } catch (e) {
            console.error(e);
            showModal();
        }
    }

    function showModal() {
        pluginContainer.find(".modal").modal('show');
    }


    async function retrieveElements() {
        const baseUrl = window.location.origin;
        let elements = await $.ajax({
            type: 'GET',
            url: `${baseUrl}/minerva/api/projects/${globals.projectId}/models/*/bioEntities/elements/`,
            dataType: 'json'
        })

        const results = {};
        elements.forEach(function (element) {
            if (results[element.name.toLowerCase()] === undefined) {
                results[element.name.toLowerCase()] = [];
            }
            results[element.name.toLowerCase()].push(new MinervaElement(element));
        });
        return results;
    }

    async function retrieveConnections(elements) {
        const baseUrl = window.location.origin;
        let connections = await $.ajax({
            type: 'GET',
            url: `${baseUrl}/minerva/api/projects/${globals.projectId}/submapConnections/`,
            dataType: 'json'
        });

        let parentElementsByModelId = [];

        connections.forEach(function (connection) {
            let targetModelId = connection.to.modelId;
            if (parentElementsByModelId[targetModelId] === undefined) {
                parentElementsByModelId[targetModelId] = getParentElementsForModelId(targetModelId, connections, elements);
            }
        });

        return parentElementsByModelId;
    }

    /**
     *
     * @param {number} targetModelId
     * @param {array} connections
     * @param {array<MinervaElement>} elements
     */
    function getParentElementsForModelId(targetModelId, connections, elements) {
        let result = [];
        let usedModelIds = [];
        usedModelIds[targetModelId] = true;

        let toCheck = [targetModelId];
        while (toCheck.length > 0) {
            let currentTargetId = toCheck.shift();
            connections.forEach(function (connection) {
                if (connection.to.modelId === currentTargetId && usedModelIds[connection.from.modelId] === undefined) {
                    result.push(getElementById(connection.from.id, elements));
                    toCheck.push(connection.to.modelId);
                    usedModelIds[connection.from.modelId] = true;
                }
            });
        }
        return result;
    }

    function getElementById(id, elements) {
        let results = elements.filter(function (element) {
            return element.id === id;
        });
        return results[0];
    }

    function getMarkerForBioEntity(bioEntity) {
        return {
            element: {
                id: bioEntity.getId(),
                modelId: bioEntity.getModelId(),
                type: 'ALIAS'
            },
            type: "ICON",
            options: {
                color: '#FF0000',
                opacity: 1
            },
            bioEntity: bioEntity
        };
    }

} catch (e) {
    console.error(e);
    throw e;
}